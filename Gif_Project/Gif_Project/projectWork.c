#include "projectWork.h"

/*
The function saves the project in the requested path as a txt file
input: the directory path, the file name
output: whether the directory exists or not
*/
int saveProject(FrameNode* list, char* directoryPath, char* fileName)
{
	FILE* toWrite = 0;
	DIR* dp = 0;
	unsigned int ch = 0;
	int directoryExists = TRUE;
	char* frameName = 0;
	unsigned int frameDuration = 0;
	char* framePath = 0;
	char* fullFilePath = (char*)malloc(STR_LEN * sizeof(char));
	if (!(dp = opendir(directoryPath))) 
	{
		directoryExists = FALSE;
	}
	else
	{
		strncat(fileName, ".txt", 1 + strlen(".txt"));
		strncat(directoryPath, "\\", 1 + strlen("\\"));
		strcpy(fullFilePath, directoryPath);
		strncat(fullFilePath, fileName, 1 + strlen(fileName));
		toWrite = fopen(fullFilePath, "w");
		while (list)
		{
			frameName = (char*)malloc(STR_LEN * sizeof(char));
			framePath = (char*)malloc(STR_LEN * sizeof(char));
			strcpy(frameName, list->frame->name);
			frameDuration = list->frame->duration;
			strcpy(framePath, list->frame->path);
			fprintf(toWrite, "%s, %d, %s\n", frameName, frameDuration, framePath);
			list = list->next;
		}
		fclose(toWrite);
	}
	return directoryExists;
}

/*
The function opens a saved project
input: the project path
output: the saved list
*/
FrameNode* openProject(char* path)
{
	FILE* fp = 0;
	FrameNode* head = NULL;
	FrameNode* prev = NULL;
	FrameNode* current = NULL;
	//Frame* newFrame = (Frame*)malloc(sizeof(Frame));
	char* buffer = (char*)malloc(512 * sizeof(char));
	int nextCammaIndex = 0;
	int secondCammaIndex = 0;
	char* newStr = (char*)malloc(STR_LEN * sizeof(char));
	char* copiedStr = (char*)malloc(1024 * sizeof(char));
	fp = fopen(path, "r");
	while (fgets(buffer, strlen(buffer), fp) && strcmp(buffer,"\n"))
	{
		current = (FrameNode*)malloc(sizeof(FrameNode));
		current->frame = (Frame*)malloc(sizeof(Frame));
		current->frame->name = (char*)malloc(STR_LEN * sizeof(char));
		current->frame->path = (char*)malloc(STR_LEN * sizeof(char));
		current->next = NULL;
		nextCammaIndex = strcspn(buffer, ",");
		newStr = strchr(buffer, ' ');
		secondCammaIndex = nextCammaIndex + strcspn(newStr, ",")+1;
		strncpy(current->frame->name, buffer, nextCammaIndex);
		current->frame->name[nextCammaIndex] = '\0';
		strncpy(copiedStr, buffer + nextCammaIndex+2, secondCammaIndex - nextCammaIndex-2);
		copiedStr[secondCammaIndex - nextCammaIndex -2] = '\0';
		current->frame->duration = atoi(copiedStr);
		strncpy(current->frame->path, buffer+secondCammaIndex+2, strlen(buffer)-secondCammaIndex);
		current->frame->path[strcspn(current->frame->path, "\n")] = '\0';
		current->frame->path[strlen(buffer) - secondCammaIndex ] = '\0';
		if (head == NULL) {
			head = current;
		}
		else
		{
			prev->next = current;
		}
		prev = current;
	}
	fclose(fp);
	return head;
}