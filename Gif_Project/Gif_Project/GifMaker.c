/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:                          *
* Credits:                       *
**********************************/

#include <stdio.h>
#include <stdlib.h>
#include "linkedList.h"
#include "view.h"
#include "printing.h"

#define FALSE 0
#define TRUE !FALSE
#define EXIT 0
#define NEW_FRAME 1
#define REMOVE_FRAME 2
#define MOVE_FRAME 3
#define CHANGE_FRAME_DURATION 4
#define CHANGE_ALL_FRAMES_DURATION 5
#define PRINT_FRAME_LIST 6
#define PLAY_GIF 7
#define SAVE_PROJECT 8

int main(void)
{
	FrameNode* head = NULL;
	FILE* fp = 0;
	char* path = (char*)malloc(STR_LEN * sizeof(char));
	unsigned int duration = 0;
	char* name = (char*)malloc(STR_LEN * sizeof(char));
	unsigned int index = FALSE;
	unsigned int choice = TRUE;
	printf("%s", WELCOME);
	printf("%s", OPEN_LOAD_MENU);
	scanf("%d", &choice);
	getchar();
	while (choice != 0 && choice != 1)
	{
		printErrors(head, INVALID_CHOICE_ERROR);
		printf("%s", OPEN_LOAD_MENU);
		scanf("%d", &choice);
		getchar();
	}
	if (choice)
	{
		printf("%s", ENTER_PROJECT_PATH_TO_OPEN);
		myFgets(path, STR_LEN);
		head = openProject(path);
		printf("\n");
	}
	else
	{
		printf("%s", NEW_PROJECT_MESSAGE);
	}
	do
	{
		printf("%s", PROJECT_MENU);
		scanf("%d", &choice);
		getchar();
		switch (choice)
		{
		case EXIT:
			freeList(&head);
			printf("%s",GOODBYE_MESSAGE);
			break;
		case NEW_FRAME:
			printf("%s", NEW_FRAME_MESSAGE);
			myFgets(path, STR_LEN);
			printf("%s", ENTER_DURATION_MESSAGE);
			scanf("%d", &duration);
			getchar();
			printf("%s", ENTER_NAME_MESSAGE);
			myFgets(name, STR_LEN);
			while (findFrame(head, name))
			{
				printErrors(head, FRAME_NAME_TAKEN);
				myFgets(name, STR_LEN);
			}
			if (fp = fopen(path, READ_MODE))
			{
				addFrame(&head, createFrame(path, duration, name));
				fclose(fp);
			}
			else
			{
				printErrors(head, FILE_PATH_ERROR);
			}
			printf("\n");
			break;
		case REMOVE_FRAME:
			printf("%s",ERASE_FRAME_MESSAGE);
			myFgets(name, STR_LEN);
			if (findFrame(head, name))
			{
				removeFrame(&head, name);
			}
			else
			{
				printErrors(head, FRAME_NOT_FOUND);
			}
			printf("\n");
			break;
		case MOVE_FRAME:
			printf("%s", NAME_OF_FRAME_MESSAGE);
			myFgets(name, STR_LEN);
			if (findFrame(head, name))
			{
				printf("%s", NEW_INDEX_MESSAGE);
				scanf("%d", &index);
				getchar();
				while (index < 1 || index > listLen(head))
				{
					printErrors(head, FRAME_BAD_INDEX);
					scanf("%d", &index);
					getchar();
				}
				moveFrame(&head, name, index);
			}
			else
			{
				printErrors(head, FRAME_DOES_NOT_EXIST);
			}
			printf("\n");
			break;
		case CHANGE_FRAME_DURATION:
			printf("%s", NAME_OF_FRAME_MESSAGE);
			myFgets(name, STR_LEN);
			if (findFrame(head, name))
			{
				printf("%s", NEW_DURATION_MESSAGE);
				scanf("%d", &duration);
				changeFrameLength(&head, name, duration);
			}
			else
			{
				printErrors(head, FRAME_DOES_NOT_EXIST);
			}
			printf("\n");
			break;
		case CHANGE_ALL_FRAMES_DURATION:
			printf("%s", ALL_FRAMES_DURATION_MESSAGE);
			scanf("%d", &duration);
			changeAllFramesLength(&head, duration);
			printf("\n");
			break;
		case PRINT_FRAME_LIST:
			printAllFrames(head);
			break;
		case PLAY_GIF:
			play(head);
			printf("\n");
			break;
		case SAVE_PROJECT:
			printf("%s", SAVE_PROJECT_MESSAGE);
			printf("%s", ENTER_PATH_MESSAGE);
			myFgets(path, STR_LEN);
			printf("%s", ENTER_NAME_TO_SAVE_MESSAGE);
			myFgets(name, STR_LEN);
			if (!saveProject(head, path, name))
			{
				printErrors(head, DIRECTORY_DOES_NOT_EXIST);
			}
			printf("\n");
			break;
		default: printErrors(head, PROJECT_MENU_CHOICE_ERROR);
		}
	} while (choice != EXIT);
	getchar();
	return 0;
}

