#ifndef PRINTINGH
#define PRINTINGH

#include <stdio.h>
#include <string.h>
#include "linkedList.h"

#define WELCOME "Welcome to Magshimim Movie Maker! what would you like to do?\n"
#define NEW_PROJECT_MESSAGE "Working on a new project"
#define ENTER_PROJECT_PATH_TO_OPEN "Enter the path of the project (including project name):\n"
#define OPEN_LOAD_MENU " [0] Create a new project\n [1] Load existing project\n"
#define PROJECT_MENU "What would you like to do?\n [0] Exit\n [1] Add new Frame\n [2] Remove a Frame\n [3] Change frame index\n [4] Change frame duration\n [5] Change duration of all frames\n [6] List frames\n [7] Play movie!\n [8] Save project\n"
#define PROJECT_MENU_CHOICE_ERROR 0
#define ERROR_0 "You should type one of the options - 0-8!\n"
#define FILE_PATH_ERROR 1
#define ERROR_1 "Can't find file! Frame will not be added"
#define FRAME_NAME_TAKEN 2
#define ERROR_2 "The name is already taken, please enter another name"
#define FRAME_NOT_FOUND 3
#define ERROR_3 "The frame was not found"
#define FRAME_DOES_NOT_EXIST 4
#define ERROR_4 "this frame does not exist"
#define FRAME_BAD_INDEX 6
#define INVALID_CHOICE_ERROR 5
#define ERROR_5 "Invalid choice, try again:"
#define DIRECTORY_DOES_NOT_EXIST 7
#define ERROR_7 "The directory name you entered does not exist\n"
#define PRINT_ERRORS_CALL_ERROR "Error! Invalid argument"
#define NEW_FRAME_MESSAGE "*** Creating new frame ***\nPlease insert frame path :\n"
#define ENTER_DURATION_MESSAGE "Please insert frame duration(in milliseconds):\n"
#define ENTER_NAME_MESSAGE "Please choose a name for that frame:\n"
#define ERASE_FRAME_MESSAGE "Enter the name of the frame you wish to erase\n"
#define NAME_OF_FRAME_MESSAGE "Enter the name of the frame\n"
#define NEW_INDEX_MESSAGE "Enter the new index in the movie you wish to place the frame\n"
#define NEW_DURATION_MESSAGE "Enter the new duration\n"
#define ALL_FRAMES_DURATION_MESSAGE "Enter duration for all frames:\n"
#define SAVE_PROJECT_MESSAGE "Where to save the project? enter a directory path and file name\n"
#define ENTER_PATH_MESSAGE "directory path: "
#define ENTER_NAME_TO_SAVE_MESSAGE "file name: "
#define GOODBYE_MESSAGE "Bye!"
#define FRAME_HEADERS "\t\tName\t\tDuration\tPath"

void printErrors(FrameNode* list, unsigned int error);
void printAllFrames(FrameNode* list);

#endif
