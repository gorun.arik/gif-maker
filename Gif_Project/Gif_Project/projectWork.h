#ifndef VIEWH
#define VIEWH

#include "linkedList.h"
#include "dirent.h"

int saveProject(FrameNode* list, char* filePath, char* fileName);
FrameNode* openProject(char* path);


#endif
