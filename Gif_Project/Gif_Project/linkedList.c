#include "linkedList.h"

/*
Function will free all memory of a list of frame nodes
input: pointer to a list of frame nodes
output: none
*/
void freeList(FrameNode** list)
{
	FrameNode* temp = NULL;
	FrameNode* curr = *list;
	while (curr)
	{
		temp = curr;
		curr = (curr)->next;
		free(temp);
	}
	*list = NULL;
}

/*
The function creates a new frame node
input: frame path, frame duration (in milliseconds), frame name
output: the new frame
*/
FrameNode* createFrame(char* path,unsigned int duration,char* name)
{
	Frame* newFrame = (Frame*)malloc(sizeof(Frame));
	FrameNode* newFrameNode = (FrameNode*)malloc(sizeof(FrameNode));
	newFrame->path = (char*)malloc(STR_LEN * sizeof(char));
	newFrame->name = (char*)malloc(STR_LEN * sizeof(char));
	strcpy(newFrame->path, path);
	newFrame->duration = duration;
	strcpy(newFrame->name, name);
	newFrameNode->frame = newFrame;
	newFrameNode->next = NULL;
	return newFrameNode;
}

/*
The function adds a given frame to the end of the list
input: pointer to a list of frames, the new frame node
output: none
*/
void addFrame(FrameNode** list, FrameNode* new)
{
	FrameNode* curr = *list;
	int nameFound = FALSE;
	if (!*list)
	{
		*list = new;
	}
	else
	{
		if (!strcmp(curr->frame->name, new->frame->name))
		{
			nameFound = TRUE;
		}
		while (curr->next && !nameFound)
		{
			if (!strcmp(curr->next->frame->name, new->frame->name))
			{
				nameFound = TRUE;
			}
			curr = curr->next;
		}
		if (!nameFound)
		{
			curr->next = new;
		}
	}
}

/*
The function finds a frame node by its name
input: a list of frames, the name of the node
output: whether the frame was found
*/
int findFrame(FrameNode* list, char* name)
{
	int found = FALSE;
	if (list && !strcmp(list->frame->name, name))
	{
		found = TRUE;
	}
	else if (list)
	{
		while (list && list->next && !found)
		{
			if (!strcmp(list->next->frame->name, name))
			{
				found = TRUE;
			}
			list = list->next;
		}
	}
	return found;
}

/*
The function removes a frame by its name
input: pointer to a list of frames
output: void
*/
void removeFrame(FrameNode** list, char* name)
{
	int found = FALSE;
	FrameNode* curr = *list;
	FrameNode* temp = NULL;
	if (!strcmp(curr->frame->name, name) && *list)
	{
		*list = curr->next;
		free(curr);
		found = TRUE;
	}
	else if(*list)
	{
		while (curr && curr->next && !found)
		{
			if (!strcmp(curr->next->frame->name, name))
			{
				temp = curr->next;
				curr->next = curr->next->next;
				free(temp);
				found = TRUE;
			}
			curr = curr->next;
		}
	}
}

/*
The function moves a frame by name and to the given index (starting at 1)
input: pointer to a list of frames, the name of the frame, the frame's new index
output: none
*/
void moveFrame(FrameNode** list, char* name, unsigned int index)
{
	int found = FALSE;
	int moved = FALSE;
	unsigned int counter = 1;
	FrameNode* curr = *list;
	FrameNode* temp1 = NULL;
	if (!strcmp(curr->frame->name, name) && *list)
	{
		temp1 = curr;
		*list = curr->next;
		found = TRUE;
	}
	else if (*list)
	{
		while (curr && curr->next && !found)
		{
			if (!strcmp(curr->next->frame->name, name))
			{
				temp1 = curr->next;
				curr->next = curr->next->next;
				found = TRUE;
			}
			curr = curr->next;
		}
	}
	curr = *list;
	if (index == 1)
	{
		temp1->next = curr;
		*list = temp1;
		moved = TRUE;
	}
	else
	{
		while (curr && curr->next && index-1 > counter)
		{
			counter++;
			curr = curr->next;
		}
		if (index-1 == counter)
		{
			temp1->next = curr->next;
			curr->next = temp1;
			moved = TRUE;
		}
	}
}

/*
The function changes the duration of a single frame
input: pointer to a list of frames, the name of the node, the duration of the node
output: none
*/
void changeFrameLength(FrameNode** list, char* name, unsigned int duration)
{
	int found = FALSE;
	FrameNode* curr = *list;
	if (!strcmp(curr->frame->name, name))
	{
		curr->frame->duration = duration;
		found = TRUE;
	}
	else
	{
		while (curr && curr->next && !found)
		{
			if (!strcmp(curr->next->frame->name, name))
			{
				curr->next->frame->duration = duration;
				found = TRUE;
			}
			curr = curr->next;
		}
	}
}

/*
The function changes all frames' duration using the changeFrameLength function
input: pointer to a list of frames, the new duration
output: none
*/
void changeAllFramesLength(FrameNode** list, unsigned int duration)
{
	FrameNode* curr = *list;
	while (curr)
	{
		changeFrameLength(list, curr->frame->name, duration);
		curr = curr->next;
	}
}

/*
The function calculates the length of a linked list
input: a list of frames
output: the length of the list
*/
int listLen(FrameNode* list)
{
	unsigned int length = 0;
	while (list)
	{
		length++;
		list = list->next;
	}
	return length;
}

/*
Function will perform the fgets command and also remove the newline
that might be at the end of the string - a known issue with fgets.
input: the buffer to read into, the number of chars to read
*/
void myFgets(char str[], int n)
{
	fgets(str, n, stdin);
	str[strcspn(str, "\n")] = 0;
}