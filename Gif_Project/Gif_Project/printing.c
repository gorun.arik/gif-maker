#include "printing.h"

/*
The function prints out errors to the screen according to which number was inputed
input: a list of frames, error - number of error
output: none
*/
void printErrors(FrameNode* list, unsigned int error)
{
	switch (error)
	{
	case PROJECT_MENU_CHOICE_ERROR: printf("%s\n", ERROR_0);
		break;
	case FILE_PATH_ERROR: printf("%s\n", ERROR_1);
		break;
	case FRAME_NAME_TAKEN: printf("%s\n", ERROR_2);
		break;
	case FRAME_NOT_FOUND: printf("%s\n", ERROR_3);
		break;
	case FRAME_DOES_NOT_EXIST: printf("%s\n", ERROR_4);
		break;
	case INVALID_CHOICE_ERROR: printf("%s\n", ERROR_5);
		break;
	case FRAME_BAD_INDEX: printf("The movie contains %d frames, starting at 1!Try again: \n", listLen(list));
		break;
	case DIRECTORY_DOES_NOT_EXIST: printf("%s", ERROR_7);
		break;
	default: printf("%s '%d'\n", PRINT_ERRORS_CALL_ERROR, error);
		return 1;
	}
}

/*
The function prints out all the frame nodes to the screen in a table form
input: a list of frames
output: none
*/
void printAllFrames(FrameNode* list)
{
	FrameNode* curr = list;
	printf("%s", FRAME_HEADERS);
	while (curr)
	{
		printf("\n\t\t%s\t\t%d ms\t\t%s", curr->frame->name, curr->frame->duration, curr->frame->path);
		curr = curr->next;
	}
	printf("\n\n\n\n");
}


