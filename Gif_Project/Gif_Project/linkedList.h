#ifndef LINKEDLISTH
#define LINKEDLISTH

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include "printing.h"

#define FALSE 0
#define TRUE !FALSE
#define READ_MODE "rb"
#define STR_LEN 50
#define FRAME_INDEX_TOO_BIG 5

// Frame struct
typedef struct Frame
{
	char*		name;
	unsigned int	duration;
	char*		path;
} Frame;


// Link (node) struct
typedef struct FrameNode
{
	Frame* frame;
	struct FrameNode* next;
} FrameNode;

void freeList(FrameNode** list);
int findFrame(FrameNode* list, char* name);
FrameNode* createFrame(char* path, unsigned int duration, char* name);
void addFrame(FrameNode** list, FrameNode* new);
void removeFrame(FrameNode** list, char* name);
void moveFrame(FrameNode** list, char* name, unsigned int index);
void changeFrameLength(FrameNode** list, char* name, unsigned int duration);
void changeAllFramesLength(FrameNode** list, unsigned int duration);
int listLen(FrameNode* list);
void myFgets(char str[], int n);

#endif
